# OSRM Chart

This chart is an implementation of the [OpenStreetMaps](http://project-osrm.org/) - providing map-related services such as routing/directions.  

It augments the [OSRM Backend](https://github.com/Project-OSRM/osrm-backend/wiki) with scripts, utilities, and a shared 
file system to support multiple maps and a non-disruptive process for map-updates.  The core implementation is the [OSRM Docker image](../../Docker/README.md)

The OSRM Chart runs in a Kubernetes cluster and requires the following capabilities:
* 

## Maps
The OSRM Chart is typically installed with one or more maps - representing distinct map regions or localities.  
Each map has a name which is used in URLs to access the service and is used in all installed component names to identify the 
components that are unique to each map.

The general convention for map names is:
```
<continent>-<region-or-country>-<state-or-province>
```
Thus the following examples:

| Map Name | Continent | Region or Country | State or Province
| -------- | --------- | ----------------- | ----------------
| na-us | North America | United States | N/A
| na-us-de | North America | United States | Deleware
| eu-germany | Europe | United States | N/A
| na-uswest | North America | United States - West | N/A

Map Names are used to derive many of the configurable aspects of the deployment.  You can see them within an OSRM implementation in the following places:
The most important of these is the url which exposes the map service.  You can 
| Item | Kubernetes Resource | Pattet
* Ingress name and path
* Service names
* Config Maps 

## Map Refresh
Maps are intended to be refreshed periodically to pick up changes to the maps which are periodically published by the OSRM community.  
As noted above, the maps come from <HERE>.   Map updates occur no less frequently than monthly.   The system's default behavior is to 
schedule a weekly refresh of the map.

Note that the system uses a blue-green deployment model for naps.  This means that it maintains two versions of every map in the
system. One is designated as the 
## Using OSRM

The OutOfTheBox OSRM API is documented at [OSRM API](http://project-osrm.org/docs/v5.5.1/api).

A you can see from looking at the API, OSRM supports a variety of different [Services](http://project-osrm.org/docs/v5.5.1/api/#services),  
but the primary one of interest for IQVIA use cases is the [Route Service](http://project-osrm.org/docs/v5.5.1/api/#route-service) - which finds fastest routes.


URLs take this form:
```
http://orsm.<domain>/<mapname>
```
For example,  for a map called "na-" installed in an the demo.iqvia.com domain,

I

## Chart Details

## Installing the Chart

To install the chart:

```bash
helm install <CHARTHOME>/osrm
```

## Forcing initial maps
Note that maps are generated periodically based on the configured schedule (customizable by map).  The default is a weekly 
update on Sunday evenings.  When OSRM is initially installed,  there are no maps... and generally you would not want to wait 
until the next interval.   So the general procedure to bootstrap the system with an initial set of maps is:
* Install the Chart
* Note that the Deployment/Map-serving-Pods will loop in an unrunnable-state and will retry conrtinuously waiting for a map to be available
* Trigger the CronJob manually (via the dashboard or  cli)


To get the list of CronJobs,  type:
```bash
kubectl get cronjobs.batch
```
For example, with a default installation:
```bash
PL2WMY15726:maps derkes$ kubectl get cronjobs.batch
NAME            SCHEDULE     SUSPEND   ACTIVE   LAST SCHEDULE   AGE
osrm-na-us-de   0 12 * * 0   False     0        <none>          5m55s

```
To trigger one of the jobs (the names should correspond to the configured maps):
```bash
kubectl create job --from=cronjob/<cronjobname> <someuniquejobname>
```
To continue the above example:
```bash
PL2WMY15726:maps derkes$ kubectl create job --from=cronjob/osrm-na-us-de osrm-na-us-de-initial
job.batch/osrm-na-us-de-initial created
```

Map creation can take from minutes to up to 3-4 hours or more - depending on several factors:
* The number and size of the maps being built.  
    * For XXL maps (8GB or more in raw PBF files,  typically entire continents),  expect 3-4 hours
## Configuration

The primary configurable in this chart is the Maps.  It supports an arbitraty number
of maps - each of which exposes an externally-callable endpoint and can be
configured independently.  

<like so>

The following table lists the configurable parameters of the chart

!TBD:  customize me

| Parameter                            | Description                                                       | Default                                                    |
| ------------------------------------ | ----------------------------------------------------------------- | ---------------------------------------------------------- |
| `zeppelin.image`                     | Zeppelin image                                                    | `dylanmei/zeppelin:{VERSION}`                              |
| `zeppelin.resources`                 | Resource limits and requests                                      | `limits.memory=4096Mi, limits.cpu=2000m`                   |
| `spark.driverMemory`                 | Memory used by [Spark driver](https://spark.apache.org/docs/latest/configuration.html#application-properties) (Java notation)  | `1g` |
| `spark.executorMemory`               | Memory used by [Spark executors](https://spark.apache.org/docs/latest/running-on-yarn.html) (Java notation)                    | `1g` |
| `spark.numExecutors`                 | Number of [Spark executors](https://spark.apache.org/docs/latest/running-on-yarn.html)                                         | `2`  |
| `hadoop.useConfigMap`                | Use external Hadoop configuration for Spark executors             | `false`                                                    |
| `hadoop.configMapName`               | Name of the hadoop config map to use (must be in same namespace)  | `hadoop-config`                                            |
| `hadoop.configPath`                  | Path in the Zeppelin image where the Hadoop config is mounted     | `/usr/hadoop-2.7.3/etc/hadoop`                             |
| `ingress.enabled`                    | Enable ingress                                                    | `false`                                                    |
| `ingress.annotations`                | Ingress annotations                                               | `{}`                                                       |
| `ingress.hosts`                      | Ingress Hostnames                                                 | `["zeppelin.local"]`                                       |
| `ingress.path`                       | Path within the URL structure                                     | `/`                                                        |
| `ingress.tls`                        | Ingress TLS configuration                                         | `[]`                                                       |
| `nodeSelecor`                        | Node selector for the Zeppelin deployment                         | `{}`                                                       |

## Related charts

The [Hadoop](https://github.com/kubernetes/charts/tree/master/stable/hadoop) chart can be used to create a YARN cluster where Spark jobs are executed:

```
helm install -n hadoop stable/hadoop
helm install --set hadoop.useConfigMap=true,hadoop.configMapName=hadoop-hadoop stable/zeppelin
```

> Note that you may also want to set the `spark.numExecutors` value to match the number of yarn NodeManager replicas and the `executorMemory` value to half of the NodeManager memory limit.
# Refreshing maps
This chart will check maps daily for updates and via a Cronjob.  This will result in updates to the non-active
blue/green map.  This process can be performed manually like this:
kubectl create job --from=<cronjobname> <cronjobname>now

## Monitoring EFS
This solution uses [Amazon's EFS](https://docs.aws.amazon.com/efs/latest/ug/whatisefs.html) or Elastic File System for
storing maps and sharing them between the various components of the system.  EFS provides near-unlimited storage and very 
good reliability out of the box.  But it is important to note the [Performance Considerations](https://docs.aws.amazon.com/efs/latest/ug/performance.html).
Most important of these is the settings around Throughput Scaling - with "Burst Mode" being the default.  This means that 
if the system uses a lot of IO for sustained periods of time, it can run into a cap.   This can generally be avoided in one of several ways:
* Ensure that map refresh schedules are staggered so that large maps not all updated at once
* configure EFS in the AWS console to have provisioned Throughput for a period of time to get oet over a cap while burst credits are replenished

It is generally a good idea to keep an eye on the relevant metrics - and to have alerts in play when burst credits drop to zero.
 Performance in that case will likely be unacceptable.
 
### Cloudwatch Dashboard Spec for Monitoring EFS
```json
{
    "widgets": [
        {
            "type": "metric",
            "x": 0,
            "y": 0,
            "width": 15,
            "height": 6,
            "properties": {
                "view": "timeSeries",
                "stacked": false,
                "metrics": [
                    [ "AWS/EFS", "BurstCreditBalance", "FileSystemId", "fs-3497f99f" ]
                ],
                "region": "us-west-2"
            }
        },
        {
            "type": "metric",
            "x": 15,
            "y": 0,
            "width": 9,
            "height": 6,
            "properties": {
                "view": "timeSeries",
                "stacked": false,
                "metrics": [
                    [ "AWS/EFS", "PermittedThroughput", "FileSystemId", "fs-3497f99f" ]
                ],
                "region": "us-west-2"
            }
        },
        {
            "type": "metric",
            "x": 15,
            "y": 6,
            "width": 9,
            "height": 6,
            "properties": {
                "view": "timeSeries",
                "stacked": false,
                "metrics": [
                    [ "AWS/EFS", "ClientConnections", "FileSystemId", "fs-3497f99f" ]
                ],
                "region": "us-west-2"
            }
        },
        {
            "type": "metric",
            "x": 0,
            "y": 6,
            "width": 15,
            "height": 6,
            "properties": {
                "view": "timeSeries",
                "stacked": false,
                "metrics": [
                    [ "AWS/EFS", "DataWriteIOBytes", "FileSystemId", "fs-3497f99f" ],
                    [ ".", "DataReadIOBytes", ".", "." ]
                ],
                "region": "us-west-2",
                "title": "IO stats"
            }
        }
    ]
}
```